
//receive new comments in realtime using Pusher as a backend
Echo.channel('comments')
    .listen('CommentSubmitted', data => {
        console.log('echo', data);
        attachComment(data.comment);
    })
    .listen('LikeCountUpdated', data => {
        updateLikeCount(data.commentId, data.likes);
    });

$(document).ready(function () {
    //open (hidden) file input when clicking on image button
    $(document.body).on('click', '.file-upload', function () {
        $(this).closest('form').find('input[name=image]').trigger('click');
    });

    //previewing image before upload
    $(document.body).on('change', '.image-input', function () {
        readImage($(this)[0], $(this).closest('form').find('.image-preview'));
    });

    //init user's username and store it in a cookie
    let username = initUsernameFromCookie();

    //add CSRF token to all AJAX requests
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //attaches reply box when adding child comments
    $(document.body).on('click', '.reply-button', function () {
        let commentBox = $(this).closest('.comment-box');
        let subComment = commentBox.find('.sub-comment');

        //show reply box if it was already created
        if(subComment.length) {
            subComment.toggle();
        } else {
            //create reply box from the template
            let source   = document.getElementById("reply-box-template").innerHTML;
            let template = Handlebars.compile(source);

            let replyBoxView = template({
                parentId: commentBox.data('comment-id')
            });

            commentBox.append(replyBoxView);
        }
    });

    //sending replies via AJAX and creating messages which are appended to the others
    $(document.body).on('click', '.post-reply-button', function () {
        let button = $(this);
        let commentBox = button.closest('.comment-box');

        let parentId = button.data('parent-id'); //ID of the comment we are replying to (can be null for main comments)

        //get text of the message
        var message;
        if(parentId) {
            message = commentBox.find('.comment-message').val();
        } else {
            message = $('#main-comment-message').val();
        }

        if(!message.length) {
            alert('Please enter your message');
            return;
        }

        button.addClass('running');
        button.prop('disabled', true);

        let imageToUpload = button.closest('form').find('input[name=image]')[0].files[0];

        let data = new FormData();
        data.append('message', message);
        data.append('name', username);
        data.append('image', imageToUpload);

        if(parentId) {
            data.append('parent_id', parentId);
        }

        $.ajax({
            type: "POST",
            url: submitCommentRoute,
            data: data,
            contentType : false,
            processData: false,
            success: function (response) {
                console.log('response ', response);

                button.removeClass('running');
                button.prop('disabled', false);

                //hide and clear message input
                $('.image-preview').attr('src', '');
                $('.image-input').val(null);

                if(parentId) {
                    let subComment = button.closest('.sub-comment')
                    subComment.find('textarea').val('');
                    subComment.hide();
                } else {
                    $('#main-comment-message').val('');
                }
            },
        });
    });

    $(document.body).on('click', '.like-button', function () {
        let commentId = $(this).closest('.comment-box').data('comment-id');

        if(Cookies.get('liked.' + commentId)) {
            alert('You have already liked this comment');
            return;
        }

        Cookies.set('liked.' + commentId, 1, {expires: 10});

        let data = {
            comment_id: commentId,
        };

        $.ajax({
            type: "POST",
            url: submitLikeRoute,
            data: data,
            success: function (response) {
                updateLikeCount(commentId, response.comment.likes);
            },
        });
    });
});

//generates random username / loads username from cookie if it already exists
//username expires in 10 days
function initUsernameFromCookie() {
    let username = Cookies.get('username');

    if (!username) {
        username = 'User ' + Math.floor(Math.random() * 10000);
        Cookies.set('username', username, {expires: 10});
    }
    return username;
}

//adds comment from parameter to the list of existing comments
function attachComment(comment) {
    //create correct message view (parent / child)
    var source;
    if(comment.parent_id) {
        source = document.getElementById("child-comment-template").innerHTML;
    } else {
        source = document.getElementById("comment-template").innerHTML;
    }

    let template = Handlebars.compile(source);

    //send message to server
    let commentView = template({
        id: comment.id,
        name: comment.name,
        message: comment.message,
        time: comment.created_at,
        imageUrl: comment.image_url,
    });

    //add comment to child comment / main comment list
    if(comment.parent_id) {
        $('#reply-list-' + comment.parent_id).prepend(commentView);
    } else {
        $('#main-comments-list').prepend(commentView);
    }
}

//updates number of likes for a particular comment
function updateLikeCount(commentId, likes) {
    $('div').find('[data-comment-id= ' + commentId + ']').find('span.like-count').text(likes + ' likes');
}

//reads image from "input" input and sets it as src to "target" img
function readImage(input, target) {
    console.log(input, target);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            target.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}