<?php

namespace App\Http\Controllers;

use App\Events\LikeCountUpdated;
use App\Model\Comment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class CommentsController extends Controller
{
    public function listComments() {
        return view('comments', [
            //selects first level comments which will come preloaded with childComments in an array
            'comments' => Comment::whereNull('parent_id')->with('childComments')->orderBy('created_at', 'DESC')->get(),
        ]);
    }

    public function submitComment() {
        $this->validate(request(), [
            'name' => 'required',
            'message' => 'required',
            'parent_id' => 'numeric|exists:comments,id'
        ]);

        $comment = new Comment();
        $comment->name = Input::get('name');
        $comment->message = Input::get('message');
        $comment->parent_id = Input::get('parent_id');

        $image = Input::file('image');

        //save image if it was sent
        if($image != null) {
            $uuid = uniqid();
            $comment->image_url = Storage::url($image->storeAs('public/images', "image-$uuid.jpg"));
        }

        $comment->save();

        return [
            'status' => 'success',
            'comment' => $comment,
        ];
    }

    public function addLike() {
        $this->validate(request(), [
            'comment_id' => 'required|exists:comments,id',
        ]);


        $comment = Comment::find(Input::get('comment_id'));
        $comment->increment('likes');

        event(new LikeCountUpdated($comment->id, $comment->likes));

        return [
            'status' => 'success',
            'comment' => $comment,
        ];
    }

    //helper function for clearing the comments
    public function clearComments() {
        Comment::truncate();
        return redirect()->route('comments.list');
    }
}
