<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Mathesio - discussion component</title>

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link href="/css/style.css" rel="stylesheet">
	<link href="/css/loading.css" rel="stylesheet">
	<link href="/css/loading-btn.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-xs-12">
			<h1>Article Title</h1>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pharetra nisl pharetra, efficitur mauris a, suscipit nunc. Maecenas in pellentesque massa, at porta massa. Fusce nec orci in urna blandit pretium. Curabitur vulputate arcu nec efficitur interdum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur condimentum mollis nunc, sit amet aliquet metus pulvinar in. Quisque non mauris at diam commodo viverra eu ac massa. Duis congue sodales massa elementum bibendum. Curabitur placerat vehicula arcu, in faucibus lorem feugiat in. Pellentesque tincidunt arcu est, nec congue purus dapibus nec. Cras in metus quam. Fusce auctor quis tortor quis blandit.
			</p>
			<p>
				Etiam posuere lectus turpis. Nunc urna urna, sodales non convallis ac, imperdiet vitae lacus. Curabitur pretium malesuada pellentesque. Vestibulum volutpat, justo sed tincidunt blandit, leo purus molestie eros, fringilla convallis diam risus a augue. In lobortis lorem eget urna vestibulum, eu pulvinar lectus consectetur. Cras euismod nisi vel dolor rhoncus, volutpat gravida lacus accumsan. In leo leo, semper id pretium ut, pharetra eu urna. Nullam purus diam, interdum nec libero eu, pharetra iaculis dolor.
			</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-xs-12">
			<h3>Comments</h3>
			<div class="widget-area no-padding blank">
				<div class="status-upload">
					<form>
						<textarea placeholder="Write your comment here..." id="main-comment-message" ></textarea>

						<a href="#" class="file-upload" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i>
						</a>

						<img class="image-preview">

						<input type="file" name="image" class="image-input" accept=".jpg">

						<button type="button" class="btn btn-success ld-ext-right post-reply-button ">
							Post comment
							<div class="ld ld-ring ld-spin"></div>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="comments-container">
			<ul id="main-comments-list" class="comments-list">

				@foreach($comments as $comment)
					<li>
						<div class="comment-main-level">
							<div class="comment-avatar"><i class="fa fa-user"></i></div>
							<div class="comment-box" data-comment-id="{{$comment->id}}">
								<div class="comment-head">
									<h6 class="comment-name by-author">{{$comment->name}}</h6>
									<span class="comment-date">{{$comment->created_at}}</span>
									<i class="fa fa-reply reply-button comment-right"></i>
									<i class="fa fa-heart like-button comment-right"></i>
									<span class="like-count comment-right">{{$comment->likes}} likes</span>
								</div>
								<div class="comment-content">
									<p>
										{{$comment->message}}
									</p>

									@if($comment->image_url)
										<img src="{{$comment->image_url}}">
									@endif
								</div>
							</div>
						</div>
						<ul class="comments-list reply-list" id="reply-list-{{$comment->id}}">

							@foreach($comment->childComments as $childComment)
								<li>
									<div class="comment-avatar"><i class="fa fa-user"></i></div>
									<div class="comment-box" data-comment-id="{{$childComment->id}}">
										<div class="comment-head">
											<h6 class="comment-name by-author">{{$childComment->name}}</h6>
											<span class="comment-date">{{$childComment->created_at}}</span>
											<i class="fa fa-heart like-button comment-right"></i>
											<span class="like-count comment-right" >{{$childComment->likes}} likes</span>
										</div>
										<div class="comment-content">
											<p>
												{{$childComment->message}}
											</p>

											@if($childComment->image_url)
												<img src="{{$childComment->image_url}}">
											@endif
										</div>
									</div>

								</li>

							@endforeach
						</ul>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>

{{--libraries--}}

<script src="/js/app.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="http://builds.handlebarsjs.com.s3.amazonaws.com/handlebars-v4.0.11.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

{{--logic for working with comments--}}

<script>
	var submitCommentRoute = "{{route('comments.submit')}}";
    var submitLikeRoute = "{{route('comments.like')}}";
</script>
<script src="/js/comments.js"></script>

{{--templates which are populated using handbars.js--}}

<script id="reply-box-template" type="text/x-handlebars-template">
	<div class="widget-area no-padding blank sub-comment">
		<div class="status-upload">
			<form>
				<textarea class="comment-message" placeholder="Write your comment here..." ></textarea>

				<a href="#" class="file-upload" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i>
				</a>

				<img class="image-preview">

				<input type="file" name="image" class="image-input" accept=".jpg">

				<button type="button" class="btn btn-success ld-ext-right post-reply-button" data-parent-id="@{{parentId}}">
					Post comment
					<div class="ld ld-ring ld-spin"></div>
				</button>
			</form>
		</div>
	</div>
</script>

<script id="comment-template" type="text/x-handlebars-template">
	<li>
		<div class="comment-main-level">
			<div class="comment-avatar"><i class="fa fa-user"></i></div>
			<div class="comment-box" data-comment-id="@{{id}}">
				<div class="comment-head">
					<h6 class="comment-name by-author">@{{name}}</h6>
					<span class="comment-date">@{{time}}</span>
					<i class="fa fa-reply reply-button comment-right"></i>
					<i class="fa fa-heart like-button comment-right"></i>
					<span class="like-count comment-right">0 likes</span>
				</div>
				<div class="comment-content">
					<p>
						@{{message}}
					</p>
					@{{#if imageUrl}}
						<img src="@{{imageUrl}}">
					@{{/if}}
				</div>
			</div>
		</div>
		<ul class="comments-list reply-list" id="reply-list-@{{id}}">
		</ul>
	</li>
</script>

<script id="child-comment-template" type="text/x-handlebars-template">
	<li>
		<div class="comment-avatar"><i class="fa fa-user"></i></div>
		<div class="comment-box" data-comment-id="@{{id}}">
			<div class="comment-head">
				<h6 class="comment-name by-author">@{{name}}</h6>
				<span class="comment-date">@{{time}}</span>
				<i class="fa fa-heart like-button comment-right"></i>
				<span class="like-count comment-right">0 likes</span>
			</div>
			<div class="comment-content">
				<p>
					@{{message}}
				</p>
				@{{#if imageUrl}}
					<img src="@{{imageUrl}}">
				@{{/if}}
			</div>
		</div>

	</li>
</script>

</body>
</html>
