<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
           $table->increments('id');
           $table->string('name');
           $table->mediumText('message');
           $table->string('image_url')->nullable();

           $table->unsignedInteger('likes')->default(0);

           $table->unsignedInteger('parent_id')->nullable();

           $table->timestamps();

           $table->foreign('parent_id')->references('id')->on('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
