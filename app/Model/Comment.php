<?php
/**
 * Created by PhpStorm.
 * User: rex
 * Date: 25.4.2018
 * Time: 13:03
 */

namespace App\Model;


use App\Events\CommentSubmitted;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Comment extends Model
{
    public static function boot ()
    {
        parent::boot();

        //send broadcast to connected clients after new comment is created
        self::created(function ($model) {
            event(new CommentSubmitted($model));
        });
    }


    public function childComments() {
        return $this->hasMany(Comment::class, 'parent_id')->orderBy('created_at', 'DESC');
    }
}