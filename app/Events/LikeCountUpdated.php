<?php
/**
 * Created by PhpStorm.
 * User: rex
 * Date: 25.4.2018
 * Time: 14:50
 */

namespace App\Events;


use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;


class LikeCountUpdated  implements ShouldBroadcast
{

    use Dispatchable, InteractsWithSockets;


    public $commentId;
    public $likes;

    public function __construct($commentId, $likes)
    {
        $this->commentId = $commentId;
        $this->likes = $likes;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     **/
    public function broadcastOn()
    {
        return new Channel('comments');
    }
}