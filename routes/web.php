<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CommentsController@listComments')->name('comments.list');
Route::get('/clear', 'CommentsController@clearComments')->name('comments.clear');
Route::post('/comments', 'CommentsController@submitComment')->name('comments.submit');
Route::post('/like', 'CommentsController@addLike')->name('comments.like');
